/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author PSYcho
 */
public class BaseServletTest {
    
    protected Servlet instance;
    
    public BaseServletTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public final void setUp() {
        instance = new Servlet();
    }
    
    @After
    public final void tearDown() {
    }

    
    
}

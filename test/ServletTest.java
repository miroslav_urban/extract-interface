
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PSYcho
 */
public class ServletTest extends BaseServletTest{
   
    @Test
    public void test200() {
        RequestInterface request = new FakeRequest("content", "asdfgh", "contacts");
        int expResult = 200;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
 
    @Test
    public void test404() {
        RequestInterface request = new FakeRequest("content", "asdfgh", "unexistentPage");
        int expResult = 404;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test400() {
        RequestInterface request = new FakeRequest(null, "asdfgh", "contacts");
        int expResult = 400;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test400b() {
        RequestInterface request = new FakeRequest("content", null, "contacts");
        int expResult = 400;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
    
     @Test
    public void test400c() {
        RequestInterface request = new FakeRequest(null, null, "contacts");
        int expResult = 400;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test401() {
        RequestInterface request = new FakeRequest("content", "asdf", "contacts");
        int expResult = 401;
        int result = instance.handleRequest(request);
        assertEquals(expResult, result);
    }
}


import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PSYcho
 */
public class Servlet {
    
    List<String> URLS = new ArrayList(){{
        add("homepage");
        add("contacts");
        add("links");
    }} ;
    
    public boolean validateHeader(String token){
        return token.length() > 5 ;
    }
    
  
    public boolean validateContent(String content){
        return content != null;
    }
    
    public int handleRequest (RequestInterface request){
        String url = request.getUrl();
        String header = request.getHeader() ;
        String content = request.getContent() ;
        if (!URLS.contains(url)){
            return 404;
        }
        if (validateContent(content)){
            return 400;
        } 
        if (!validateHeader(header)){
            return 401;
        }
        return 200;
    } 
}

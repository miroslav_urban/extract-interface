package assignment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PSYcho
 */
public final class HttpRequest {
    
    
    private String content;
    private String header;
    private String url;
    
    private HttpRequest(){
    }
    
    public String getUrl(){
        return this.url;
    }
    
    public String getContent(){
        return this.content;
    }
    
    public String getHeader(){
        return this.header;
    }
}



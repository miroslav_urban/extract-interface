package assignment;

import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author PSYcho
 */
public class Servlet {

    public static final String[] URLS = new String[] { "contacts", "index", "about", "news", "admin" };

    public boolean validateHeader(String header) {
        return header != null && header.length() > 5;
    }

    public boolean validateContent(String content) {
        return content != null;
    }
    
    public boolean validateUrl(String url){
        return Arrays.asList(URLS).contains(url);
    }

    public int handleRequest(HttpRequest request) {
        String url = request.getUrl();
        String header = request.getHeader();
        String content = request.getContent();
        if (!validateUrl(url)){
            return 404;
        }
        if (!validateHeader(header)) {
            return 401;
        }
        if (validateContent(content)) {
            return 400;
        }
        return 200;
    }

}

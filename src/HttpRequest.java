/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PSYcho
 */
public class HttpRequest implements RequestInterface {
    
    private String content;
    private String header;
    private String url;
    
    private HttpRequest(){
    }
    
    @Override
    public String getUrl(){
        return this.url;
    }
    
    @Override
    public String getContent(){
        return this.content;
    }
    
    @Override
    public String getHeader(){
        return this.header;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PSYcho
 */
public class FakeRequest implements RequestInterface{
    
    private String content, header, url;

    public FakeRequest(String content, String header, String url){
        this.content = content;
        this.header = header;
        this.url = url;
    }
    
    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public String getUrl() {
        return url;
    }
    
}
